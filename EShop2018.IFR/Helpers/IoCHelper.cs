﻿using EShop2018.IFR.IoC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;

namespace EShop2018.IFR.Helpers
{
    public static class IoCHelper
    {
        public static T Resolve<T>()
        {
            return IoC.UnityConfig.Container.Resolve<T>();
        }
    }
}
