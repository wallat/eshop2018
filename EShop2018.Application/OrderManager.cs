﻿using EShop2018.CORE.Domain;
using EShop2018.CORE.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop2018.Application
{
    /// <summary>
    /// Manager de pedidos
    /// </summary>
    public class OrderManager : Manager<Order>, IOrderManager
    {
        /// <summary>
        /// Constructor del manager
        /// </summary>
        /// <param name="context">Contexto de datos</param>
        public OrderManager(IDbContext context)
            :base(context)
        {

        }
    }
}
