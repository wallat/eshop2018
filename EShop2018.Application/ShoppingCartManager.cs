﻿using EShop2018.CORE.Domain;
using EShop2018.CORE.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace EShop2018.Application
{
    /// <summary>
    /// Manager de carrito
    /// </summary>
    public class ShoppingCartManager : Manager<ShoppingCartLine>, IShoppingCartManager
    {
        /// <summary>
        /// Constructor del manager
        /// </summary>
        /// <param name="context">Contexto de datos</param>
        public ShoppingCartManager(IDbContext context)
            :base(context)
        {

        }

        /// <inheritdoc/>
        public decimal AddProduct(string userId, string sessionId, int productId, decimal quantity)
        {
            var query = GetShoppingCartByUser(userId, sessionId).Where(e => e.ProductId == productId);
            if (query.Any())
            {
                query.Single().Quantity = query.Single().Quantity + quantity;
                query.Single().Date = DateTime.Now;
            }
            else
            {
                this.Context.ShoppingCart.Add(new ShoppingCartLine {
                     Date = DateTime.Now,
                     ProductId = productId,
                     Quantity = quantity,
                     UserId = userId,
                     Session = string.IsNullOrWhiteSpace(userId) ? sessionId : null
                });
            }
            this.Context.SaveChanges();
            return GetShoppingCartByUser(userId, sessionId).Sum(e=> e.Quantity);
        }

        /// <inheritdoc/>
        public IQueryable<ShoppingCartLine> GetShoppingCartByUser(string userId, string sessionId)
        {
            var query = this.GetAll().Include(e=> e.Product);
            if (!string.IsNullOrWhiteSpace(userId))            
                query = query.Where(e => e.UserId == userId);            
            else            
                query = query.Where(e => e.Session == sessionId);
            
            return query;
        }        
    }
}
