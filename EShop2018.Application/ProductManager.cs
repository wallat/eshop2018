﻿using EShop2018.CORE.Domain;
using EShop2018.CORE.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop2018.Application
{
    /// <summary>
    /// Manager de producto
    /// </summary>
    public class ProductManager : Manager<Product>, IProductManager
    {
        /// <summary>
        /// Constructor de manager
        /// </summary>
        /// <param name="context"></param>
        public ProductManager(IDbContext context)
            :base(context)
        {

        }
        
        /// <inheritdoc/>
        public IQueryable<Product> GetByCategoryId(int categoryId)
        {
            return Context.Products.Where(p => p.Categories.Where(c => c.Id == categoryId).Any());
        }
    }
}
