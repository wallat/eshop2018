﻿using EShop2018.CORE.Domain;
using EShop2018.CORE.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop2018.Application
{
    /// <summary>
    /// Manager de tasas
    /// </summary>
    public class TaxManager : Manager<Tax>, ITaxManager
    {
        /// <summary>
        /// Constructor del manager
        /// </summary>
        /// <param name="context">Contexto de datos</param>
        public TaxManager(IDbContext context)
            :base(context)
        {

        }

        /// <inheritdoc/>
        public decimal GetFinalPrice(int taxId, decimal price)
        {
            Tax tax = Context.Taxs.Find(taxId);
            return price * (1+(tax.Percent/100));
        }

        /// <inheritdoc/>
        public decimal GetNetPrice(int taxId, decimal price)
        {
            Tax tax = Context.Taxs.Find(taxId);
            return GetNetPrice(tax.Percent, price);
        }

        /// <inheritdoc/>
        public decimal GetNetPrice(decimal tax, decimal price)
        {
            return price / (1 + (tax / 100));
        }
    }
}
