﻿using EShop2018.CORE.Domain;
using EShop2018.CORE.Interfaces;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop2018.Application
{
    public class UserManager : Microsoft.AspNet.Identity.UserManager<ApplicationUser>, IUserManager
    {
        public UserManager(IDbContext context):base(new UserStore<ApplicationUser>((DbContext)context))
        {

        }
    }
}
