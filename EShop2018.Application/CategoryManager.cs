﻿using EShop2018.CORE.Domain;
using EShop2018.CORE.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop2018.Application
{
    /// <summary>
    /// Manager de categorias
    /// </summary>
    public class CategoryManager : Manager<Category>, ICategoryManager
    {
        /// <summary>
        /// Constructor del manager
        /// </summary>
        /// <param name="context">contexto de datos</param>
        public CategoryManager(IDbContext context)
            :base(context)
        {

        }
    }
}
