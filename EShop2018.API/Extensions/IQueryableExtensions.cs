﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Linq.Dynamic;

namespace EShop2018.API.Extensions
{
    /// <summary>
    /// Métodos extensibles para IQueryable
    /// </summary>
    public static class IQueryableExtensions
    {
        /// <summary>
        /// Método para paginar un IQueryable
        /// </summary>
        /// <typeparam name="T">Tipo de objeto del IQueryable</typeparam>
        /// <param name="this">Objeto a paginar</param>
        /// <param name="orderby">Campo por el que queremos ordenar</param>
        /// <param name="orderdir">Direccion de ordenación asc  o desc</param>
        /// <param name="take">Número de elementos a coger</param>
        /// <param name="skip">Número de elementos a saltar</param>
        /// <returns></returns>
        public static IQueryable<T> Paginate<T>
            (this IQueryable<T> @this, 
            string orderby, string orderdir = "", int take = int.MaxValue, int skip = 0)
        {
            //Si no me viene el campo por el que ordenar, no pagino
            if(string.IsNullOrWhiteSpace(orderby))
                return @this;

            if (string.IsNullOrWhiteSpace(orderdir) || orderdir == "asc")
                orderdir = "";

            @this = @this.OrderBy(orderby + " " + orderdir);
            @this = @this.Skip(skip);
            @this = @this.Take(take);

            return @this;
        }
    }
}