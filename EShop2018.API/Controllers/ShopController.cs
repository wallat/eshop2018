﻿using AutoMapper.QueryableExtensions;
using EShop2018.CORE.Domain;
using EShop2018.CORE.Interfaces;
using EShop2018.DTOs;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EShop2018.API.Controllers
{
    /// <summary>
    /// Controlador de tienda
    /// </summary>
    [RoutePrefix("api/Shop")]
    public class ShopController : ApiController
    {
        IShoppingCartManager cartManager;
        
        /// <summary>
        /// Constructor de la clase
        /// </summary>
        /// <param name="cartManager">Manager de carrito</param>
        public ShopController(IShoppingCartManager cartManager)
        {
            this.cartManager = cartManager;
        }

        /// <summary>
        /// Método para añadir al carrito
        /// </summary>
        /// <param name="productId">Identificador de producto</param>
        /// <param name="quantity">Cantidad a añadir</param>
        /// <param name="sessionId">Indentificador de session</param>
        /// <returns>Elementos que tiene el carrito</returns>
        [HttpPost]
        [Route("AddProduct/{productId}/{quantity}/{sessionId}")]
        public decimal AddProduct(int productId, int quantity, string sessionId)
        {

            return cartManager.AddProduct(
                User.Identity.IsAuthenticated ? User.Identity.GetUserId() : null,
                sessionId,
                productId,
                quantity);
        }

        /// <summary>
        /// Obtiene el carrito de un usuario
        /// </summary>
        /// <param name="sessionId">Identificador de sessión por si no está autenticado</param>
        /// <returns>Lista del productos del carrito</returns>
        [HttpGet]
        [Route("Cart/{sessionId}")]
        public IEnumerable<ShoppingCartLineDTO> GetCart(string sessionId)
        {
            return cartManager.GetShoppingCartByUser(
                User.Identity.IsAuthenticated ? User.Identity.GetUserId() : null,
                sessionId
            ).ToList().AsQueryable().ProjectTo<ShoppingCartLineDTO>().ToList();
        }

        /// <summary>
        /// Elimina una linea del carrito
        /// </summary>
        /// <param name="id">Identificador de la linea a borrar</param>
        /// <param name="sessionId">Indentificador de session</param>
        [HttpDelete]
        [Route("Cart/{id}/{sessionId}")]
        public void Delete(int id, string sessionId)
        {
            var line = cartManager.GetShoppingCartByUser(
                User.Identity.IsAuthenticated ? User.Identity.GetUserId() : null,
                sessionId
            ).Where(e => e.Id == id).SingleOrDefault();

            if(line!=null)
            {
                cartManager.Remove(line);
                cartManager.SaveChanges();
            }
            else
            {
                throw new KeyNotFoundException();
            }
        }
    }
}
