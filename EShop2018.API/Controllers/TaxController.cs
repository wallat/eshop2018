﻿using AutoMapper.QueryableExtensions;
using EShop2018.CORE.Interfaces;
using EShop2018.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EShop2018.API.Controllers
{
    /// <summary>
    /// Controlador de impuestos
    /// </summary>
    [RoutePrefix("api/Tax")]
    public class TaxController : ApiController
    {
        ITaxManager taxManager = null;

        /// <summary>
        /// Constructor del controlador
        /// </summary>
        /// <param name="taxManager">Manager de impuestos</param>
        public TaxController(ITaxManager taxManager)
        {
            this.taxManager = taxManager;
        }

        /// <summary>
        /// Obtiene todos los impuestos
        /// </summary>
        /// <returns>Lista de impuestos</returns>
        [Route("")]
        public IEnumerable<TaxDTO> GetAll()
        {
            return taxManager.GetAll().ProjectTo<TaxDTO>();
        }
    }
}
