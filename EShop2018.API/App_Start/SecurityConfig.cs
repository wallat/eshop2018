﻿using EShop2018.CORE.Domain;
using EShop2018.CORE.Interfaces;
using EShop2018.IFR.Helpers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace EShop2018.API
{
    /// <summary>
    /// Clase para configurar la seguridad por defecto
    /// </summary>
    public static class SecurityConfig
    {
        private const string RolAdmin = "Admin";
        private const string UserAdmin = "admin@eshop2018.com";
        private const string PasswordAdmin = "123QWEasd@";

        /// <summary>
        /// Configura la seguridad
        /// </summary>
        public static void Configure()
        {
            //Configuramos seguridad
            var roleManager = IoCHelper.Resolve<IRoleManager>() as RoleManager<IdentityRole>;
            var userManager = IoCHelper.Resolve<IUserManager>() as UserManager<ApplicationUser>;

            //Si no existe el rol Admin, lo crea
            if (!roleManager.RoleExists(RolAdmin))
                roleManager.Create(new IdentityRole(RolAdmin));           

            //Buscamos al usuario Admin, si no está lo creamos
            ApplicationUser user = userManager.FindByName(UserAdmin);
            if (user == null)
            {
                user = new ApplicationUser();
                user.UserName = UserAdmin;
                user.Email = UserAdmin;
                user.NIF = "0000000";
                user.Name = "Super";
                user.Surnames = "Admin";
                try
                {
                    IdentityResult result = userManager.Create(user, PasswordAdmin);
                    if (result.Succeeded)
                    {
                        userManager.AddToRole(user.Id, RolAdmin);
                    }
                    else
                    {
                        throw new Exception(string.Join(",", result.Errors));
                        //TODO: Escribir en el log
                    }
                }
                catch (Exception ex)
                {
                    //TODO: Escribir en el log
                    throw ex;
                }
            }
            else
            {
                //El usuario está creado, ¿Pero ya esta en el rol admin?
                if (!userManager.IsInRole(user.Id, RolAdmin))
                {
                    userManager.AddToRole(user.Id, RolAdmin);                    
                }
            }
        }
    }
}