﻿using AutoMapper;
using EShop2018.API.Models;
using EShop2018.CORE.Domain;
using EShop2018.CORE.Interfaces;
using EShop2018.DTOs;
using EShop2018.IFR.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EShop2018.API
{
    /// <summary>
    /// Configurador de Autommaper
    /// </summary>
    public static class AutomapperConfig
    {
        /// <summary>
        /// Método para configurar autommaper
        /// </summary>
        public static void Configure()
        {            
            var taxManager = IoCHelper.Resolve<ITaxManager>();
            var categoryManager = IoCHelper.Resolve<ICategoryManager>();

            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<RegisterDTO, RegisterBindingModel>();
                cfg.CreateMap<ShoppingCartLine, ShoppingCartLineDTO>()
                    .ForMember(
                        dest => dest.Name,
                        opt => opt.MapFrom(src => src.Product.Name)
                    )
                    .ForMember(
                        dest => dest.Price,
                        opt => opt.MapFrom(src => src.Product.Discount == null
                                 ? taxManager.GetFinalPrice(src.Product.Tax_Id, src.Product.Price)
                                 : (decimal?)taxManager.GetFinalPrice(src.Product.Tax_Id,
                                        taxManager.GetNetPrice((decimal)src.Product.Discount, src.Product.Price)))
                    );
                cfg.CreateMap<ProductDTO, Product>();
                cfg.CreateMap<ProductCreateDTO, Product>()
                    .ForMember(dest => dest.Categories,
                    opt => opt.MapFrom(src => src.Categories == null
                            ? null
                            : categoryManager.GetByIds(
                                src.Categories.Select(i=> (object)i).ToList()
                            )));
                cfg.CreateMap<Product, ProductDTO>()
                    .ForMember(
                        dest => dest.Price,
                        opt => opt.MapFrom(src =>
                                src.Discount == null
                                 ? taxManager.GetFinalPrice(src.Tax_Id, src.Price)
                                 : (decimal?)taxManager.GetFinalPrice(src.Tax_Id,
                                        taxManager.GetNetPrice((decimal)src.Discount, src.Price))))
                     .ForMember(
                        dest => dest.OldPrice,
                        opt => opt.MapFrom(src =>
                                src.Discount == null
                                    ? null
                                    : (decimal?)taxManager.GetFinalPrice(src.Tax_Id, src.Price)))
                     .ForMember(
                        dest => dest.Image,
                        opt => opt.MapFrom(src =>
                            src.Documents.Where(i => i.Type == CORE.Enums.DocumentType.Image).Any()
                            ? src.Documents.Where(i => i.Type == CORE.Enums.DocumentType.Image).OrderBy(i => i.Order)
                                .First().Path
                            : "")
                        );
                cfg.CreateMap<Tax, TaxDTO>();
            });            
        }
    }
}