﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EShop2018.MVC.Security
{
    /// <summary>
    /// Interfaz de la sesion de usuario
    /// </summary>
    public interface IUserSession
    {
        /// <summary>
        /// Nombre de usuario
        /// </summary>
        string Username { get; }
        /// <summary>
        /// Token
        /// </summary>
        string BearerToken { get; }
        /// <summary>
        /// Roles
        /// </summary>
        List<string> Roles { get; }
    }
}