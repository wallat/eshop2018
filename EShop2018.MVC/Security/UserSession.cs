﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;

namespace EShop2018.MVC.Security
{
    /// <summary>
    /// Clase de sesión de usuario
    /// </summary>
    public class UserSession : IUserSession
    {
        /// <inheritdoc/>
        public string Username
        {
            get
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                    return ((ClaimsPrincipal)HttpContext.Current.User).FindFirst(ClaimTypes.Name).Value;
                return null;
            }
        }

        /// <inheritdoc/>
        public string BearerToken
        {
            get {
                if(HttpContext.Current.User.Identity.IsAuthenticated)
                    return ((ClaimsPrincipal)HttpContext.Current.User).FindFirst("AcessToken").Value;
                return null;
            }
        }

        /// <inheritdoc/>
        public List<string> Roles
        {
            get
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                    return ((ClaimsPrincipal)HttpContext.Current.User)
                        .FindAll(ClaimTypes.Role).Select(e=> e.Value).ToList();

                return null;
            }
        }
    }
}