﻿using EShop2018.MVC.Constants;
using EShop2018.MVC.Security;
using Microsoft.Owin.Security;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Web;

namespace EShop2018.MVC.Utils
{
    public class Connect
    {
        public string Url { get; private set; }

        public Connect()
        {
            this.Url = ConfigurationManager.AppSettings["ApiBaseUri"];
        }
        public Connect(string username, string password)
            : this()
        {
            GetToken(username, password);
        }

        public IUserSession UserSession
        {
            get
            {
                return new UserSession();
            }
        }

        private void GetToken(string username, string password)
        {
            var getTokenUrl = string.Format(ApiEndPoints.GetToken, Url);

            using (HttpClient httpClient = new HttpClient())
            {
                HttpContent content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("grant_type", "password"),
                    new KeyValuePair<string, string>("username", username),
                    new KeyValuePair<string, string>("password", password)
                });

                HttpResponseMessage result = httpClient.PostAsync(getTokenUrl, content).Result;

                string resultContent = result.Content.ReadAsStringAsync().Result;

                var token = JsonConvert.DeserializeObject<Token>(resultContent);

                AuthenticationProperties options = new AuthenticationProperties();

                options.AllowRefresh = true;
                options.IsPersistent = true;
                options.ExpiresUtc = DateTime.UtcNow.AddSeconds(int.Parse(token.expires_in));

                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, token.UserName),
                    new Claim("AcessToken", string.Format("Bearer {0}", token.access_token)),                    
                };
                claims.AddRange(token.Roles.Split(',').Select(e => new Claim(ClaimTypes.Role, e)));
                

                var identity = new ClaimsIdentity(claims, "ApplicationCookie");

                HttpContext.Current.Request.GetOwinContext().Authentication.SignIn(options, identity);
            }
        }

        public T Get<T>(string action)            
        {            
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add
                    (new MediaTypeWithQualityHeaderValue("application/json"));
                if(UserSession.BearerToken!=null)
                    httpClient.DefaultRequestHeaders.Add("Authorization", UserSession.BearerToken);

                HttpResponseMessage result = httpClient.GetAsync(Url + action).Result;
                string resultContent = result.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<T>(resultContent.Replace("m_Item1","Item1").Replace("m_Item2", "Item2"));
            }
        }

        public T Post<T>(string action, object value)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add
                    (new MediaTypeWithQualityHeaderValue("application/json"));
                if (UserSession.BearerToken != null)
                    httpClient.DefaultRequestHeaders.Add("Authorization", UserSession.BearerToken);

                StringContent content = null;
                if (value != null)
                    content = new StringContent(JsonConvert.SerializeObject(value), Encoding.UTF8, "application/json");

                HttpResponseMessage result = httpClient.PostAsync(Url + action, content).Result;
                string resultContent = result.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<T>(resultContent.Replace("m_Item1", "Item1").Replace("m_Item2", "Item2"));
            }
        }
        public T Put<T>(string action, object value)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add
                    (new MediaTypeWithQualityHeaderValue("application/json"));
                if (UserSession.BearerToken != null)
                    httpClient.DefaultRequestHeaders.Add("Authorization", UserSession.BearerToken);

                StringContent content = null;
                if (value != null)                    
                    content = new StringContent(JsonConvert.SerializeObject(value), Encoding.UTF8, "application/json");

                HttpResponseMessage result = httpClient.PutAsync(Url + action, content).Result;
                string resultContent = result.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<T>(resultContent.Replace("m_Item1", "Item1").Replace("m_Item2", "Item2"));
            }
        }

        public void Delete(string action)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add
                    (new MediaTypeWithQualityHeaderValue("application/json"));
                if (UserSession.BearerToken != null)
                    httpClient.DefaultRequestHeaders.Add("Authorization", UserSession.BearerToken);

                HttpResponseMessage result = httpClient.DeleteAsync(Url + action).Result;                
            }
        }

    }
}