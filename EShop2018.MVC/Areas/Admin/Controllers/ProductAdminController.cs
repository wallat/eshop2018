﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using EShop2018.DTOs;
using EShop2018.MVC.Areas.Admin.Models.ProductAdmin;
using EShop2018.MVC.Constants;
using EShop2018.MVC.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EShop2018.MVC.Areas.Admin.Controllers
{
    [Authorize(Roles="Admin")]
    public class ProductAdminController : BaseController
    {
        // GET: Admin/ProductAdmin        
        public ActionResult Index()
        {
            var products = Connect.Get<Tuple<int, IEnumerable<ProductDTO>>>
                (string.Format(ApiEndPoints.GetProducts, "Id", "asc", int.MaxValue, 0));
            return View(products.Item2.AsQueryable().ProjectTo<ProductViewModel>());
        }
        

        // GET: Admin/ProductAdmin/Create
        public ActionResult Create()
        {
            var model = PrepareProductViewModel(null);
            return View(model);
        }

        // POST: Admin/ProductAdmin/Create
        [HttpPost]
        public ActionResult Create(ProductViewModel product)
        {
            try
            {
                var dto = Mapper.Map<ProductCreateDTO>(product);
                var id = Connect.Put<int>(ApiEndPoints.AddProduct, dto);

                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {                
                ModelState.AddModelError("", "Se ha producido un error");
                var model = PrepareProductViewModel(product);
                return View(model);
            }
        }

        // GET: Admin/ProductAdmin/Edit/5
        public ActionResult Edit(int id)
        {
            var product = Connect.Get<ProductDTO>(String.Format(ApiEndPoints.GetProduct, id));
            var model = Mapper.Map<ProductViewModel>(product);
            model = PrepareProductViewModel(model);

            return View(model);
        }

        // POST: Admin/ProductAdmin/Edit/5
        [HttpPost]
        public ActionResult Edit(ProductViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    // TODO: Add update logic here

                    return RedirectToAction("Index");
                }
                else
                {
                    model = PrepareProductViewModel(model);
                    return View(model);
                }
            }
            catch
            {
                return View();
            }
        }

        public ProductViewModel PrepareProductViewModel(ProductViewModel model)
        {
            if (model == null)
                model = new ProductViewModel();

            #region taxs
            var taxs = Connect.Get<List<TaxDTO>>(ApiEndPoints.GetAllTax);
            model.Taxs = taxs.Select(e => new SelectListItem
            {
                Text = e.Name,
                Value = e.Id.ToString()
            }).ToList();
            #endregion
            #region Categorias
            var categories = Connect.Get <List<CategoryDTO>>(ApiEndPoints.GetCategories);
            model.CategoryList = categories.Select(e => new SelectListItem
            {
                Text = e.Name,
                Value = e.Id.ToString()
            }).ToList();
            #endregion

            return model;
        }

        // GET: Admin/ProductAdmin/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Admin/ProductAdmin/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
