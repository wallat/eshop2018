﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EShop2018.MVC.Areas.Admin.Models.ProductAdmin
{
    /// <summary>
    /// ViewModel de edición de producto
    /// </summary>
    public class ProductViewModel
    {
        /// <summary>
        /// Identificador del producto
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Nombre del producto
        /// </summary>
        [Display(Name="Nombre")]
        [Required(ErrorMessage ="El nombre es requerido")]
        [MinLength(10, ErrorMessage ="El nombre debe ser de al menos 10 carácteres")]
        public string Name { get; set; }

        /// <summary>
        /// Descripción corta del producto
        /// </summary>
        [Display(Name="Pequeña Descripción")]
        public string SortDescription { get; set; }

        /// <summary>
        /// Descripción larga del producto
        /// </summary>
        [Display(Name="Descripción")]
        public string Description { get; set; }

        /// <summary>
        /// Precio neto del producto
        /// </summary>
        [Display(Name="Precio Neto")]
        [Required(ErrorMessage = "El precio es requerido")]
        [Range(0,int.MaxValue)]
        public decimal Price { get; set; }

        /// <summary>
        /// Stock
        /// </summary>
        [Display(Name="Stock")]
        [Required(ErrorMessage ="El stock es requerido")]
        [Range(0, int.MaxValue)]
        public decimal Stock { get; set; }

        /// <summary>
        /// Disponible
        /// </summary>
        [Display(Name="Activo")]
        public bool Available { get; set; }

        /// <summary>
        /// Iva del producto
        /// </summary>
        [Display(Name="IVA")]
        [Required(ErrorMessage ="El iva es requerido")]
        public int Tax_Id { get; set; }

        /// <summary>
        /// Lista de ivas
        /// </summary>
        public List<SelectListItem> Taxs { get; set; }

        /// <summary>
        /// Destacado
        /// </summary>
        [Display(Name="Destacado")]
        public bool Featured { get; set; }

        /// <summary>
        /// Descuento
        /// </summary>
        [Display(Name="Descuento")]
        public decimal? Discount { get; set; }

        public List<SelectListItem> CategoryList { get; set; }
        [Display(Name = "Categorias")]
        public List<string> Categories { get; set; }
    }
}