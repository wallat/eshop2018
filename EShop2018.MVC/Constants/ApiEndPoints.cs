﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EShop2018.MVC.Constants
{
    /// <summary>
    /// Clase estática con los métodos del api
    /// </summary>
    public static class ApiEndPoints
    {
        /// <summary>
        /// Método para obtener el token
        /// </summary>
        public const string GetToken = "{0}/Token";

        /// <summary>
        /// URL para obtener todos los productos
        /// </summary>
        public const string GetProducts = "/api/Product/?orderby={0}&orderdir={1}&take={2}&skip={3}";

        /// <summary>
        /// URL para obtener las categorías
        /// </summary>
        public const string GetCategories = "/api/Category";

        /// <summary>
        /// URL para añadir un producto al carrito
        /// </summary>
        public const string PostProduct = "/api/Shop/AddProduct/{0}/{1}/{2}";

        /// <summary>
        /// URL para obtener el carrito
        /// </summary>
        public const string GetShoppingCartLines = "/api/Shop/Cart/{0}";

        /// <summary>
        /// Eliminar una línea del carrito
        /// </summary>
        public const string DeleteCartLine = "/api/Shop/Cart/{0}/{1}";

        public const string GetProduct = "/api/Product/{0}";

        /// <summary>
        /// Obtiene todas las tasas
        /// </summary>
        public const string GetAllTax = "/api/Tax";

        /// <summary>
        /// Añadir un producto
        /// </summary>
        public const string AddProduct = "/api/Product/Add";

        /// <summary>
        /// Ruta para registrar
        /// </summary>
        public const string Register = "/api/Account/Register";
    }
}