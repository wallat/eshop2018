﻿using EShop2018.MVC.Constants;
using EShop2018.MVC.Security;
using EShop2018.MVC.Utils;
using Microsoft.Owin.Security;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace EShop2018.MVC.Controllers
{    
    public class HomeController : Controller
    {
        private readonly IUserSession _userSession;

        public HomeController()
        {
            _userSession = new UserSession();
        }

        public ActionResult Index()
        {           
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}