﻿using AutoMapper.QueryableExtensions;
using EShop2018.DTOs;
using EShop2018.MVC.Constants;
using EShop2018.MVC.Models.Cart;
using EShop2018.MVC.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EShop2018.MVC.Controllers
{
    /// <summary>
    /// Controlador de Tienda
    /// </summary>
    public class ShopController : BaseController
    {
        /// <summary>
        /// Añade un producto a la cesta
        /// </summary>
        /// <param name="productId">Identificador de producto</param>
        /// <returns>Número de productos que contiene la cesta</returns>
        public ActionResult AddCartItem(int productId, int quantity = 1)
        {
            var count = Connect.Post<decimal>(
                string.Format(ApiEndPoints.PostProduct, productId, quantity, Session.SessionID),
                null
            );
            return Content(count.ToString());
        }

        /// <summary>
        /// Carrito del usuario
        /// </summary>
        /// <returns>Carrito</returns>
        public ActionResult GetCart()
        {
            var shoppingCartLines = Connect.Get<List<ShoppingCartLineDTO>>(
                string.Format(ApiEndPoints.GetShoppingCartLines, Session.SessionID))
                .AsQueryable().ProjectTo<CartLineViewModel>();
            return Json(shoppingCartLines, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Cart()
        {
            var shoppingCartLines = Connect.Get<List<ShoppingCartLineDTO>>(
               string.Format(ApiEndPoints.GetShoppingCartLines, Session.SessionID))
               .AsQueryable().ProjectTo<CartLineViewModel>();
            return View(shoppingCartLines);
        }

        public ActionResult Delete(int id)
        {
            Connect.Delete(string.Format(ApiEndPoints.DeleteCartLine, id, Session.SessionID));
            return RedirectToAction("Cart");
        }
        
    }
}