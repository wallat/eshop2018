﻿using AutoMapper.QueryableExtensions;
using EShop2018.DTOs;
using EShop2018.MVC.Constants;
using EShop2018.MVC.Models.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EShop2018.MVC.Controllers
{
    public class ProductController : BaseController
    {
        // GET: Product
        public ActionResult Index(string orderby, string orderdir, int? take, int? skip)
        {
            var model = new ProductIndexViewModel();
            var products = Connect.Get<Tuple<int,IEnumerable<ProductDTO>>>(
                String.Format(ApiEndPoints.GetProducts, orderby, orderdir, take, skip)
            );
            var categories = Connect.Get<IEnumerable<CategoryDTO>>(
                ApiEndPoints.GetCategories
            );

            model.Products = products.Item2;
            model.ProductCount = products.Item1;
            model.Categories = categories;

            return View(model);
        }

        // GET: Product/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Product/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Product/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Product/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Product/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Product/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Product/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
