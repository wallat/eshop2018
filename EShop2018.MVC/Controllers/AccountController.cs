﻿using AutoMapper;
using EShop2018.DTOs;
using EShop2018.MVC.Constants;
using EShop2018.MVC.Models;
using EShop2018.MVC.Models.Account;
using EShop2018.MVC.Utils;
using Microsoft.Owin.Security;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace EShop2018.MVC.Controllers
{
    /// <summary>
    /// Implementacion como https://stackoverflow.com/questions/39465983/where-to-store-bearer-token-in-mvc-from-web-api
    /// </summary>
    public class AccountController : BaseController
    {
        // GET: Account
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(LoginViewModel model)
        {
            Connect connect = new Connect(model.UserName, model.Password);
            
            return RedirectToAction("Index", "Home");
        }

        public ActionResult LogOff()
        {
            Request.GetOwinContext().Authentication.SignOut("ApplicationCookie");

            return RedirectToAction("Login");
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(RegisterViewModel model)
        {
            try
            {
                RegisterDTO dto = Mapper.Map<RegisterDTO>(model);
                this.Connect.Post<bool>(ApiEndPoints.Register, dto);
                return RedirectToAction("Login");
            }
            catch(Exception ex)
            {
                ModelState.AddModelError("", "Se ha producido el siguiente error: " + ex.Message);
                return View(model);
            }
        }
        
    }
}
