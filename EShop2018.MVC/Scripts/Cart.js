﻿/*
	Add to cart fly effect with jQuery. - May 05, 2013
	(c) 2013 @ElmahdiMahmoud - fikra-masri.by
	license: https://www.opensource.org/licenses/mit-license.php
*/
$('.add-to-cart').on('click', function () {
    var productId = $(this).data("id");
    $.ajax({
        type: "POST",
        url: "/Shop/AddCartItem",
        data: { productId: productId },
        success: function (data)
        {
            effect(productId);
            setTimeout(function () {
                $(".cartItems").text(parseInt(data));
            }, 1500);
        }
    });
});
function effect(productId) {
    var cart = $('.shopping-cart');
    var imgtodrag = $(".Image_" + productId);
    if (imgtodrag) {
        var imgclone = imgtodrag.clone()
            .offset({
                top: imgtodrag.offset().top,
                left: imgtodrag.offset().left
            })
            .css({
                'opacity': '0.5',
                'position': 'absolute',
                'height': '150px',
                'width': '150px',
                'z-index': '100'
            })
            .appendTo($('body'))
            .animate({
                'top': cart.offset().top + 10,
                'left': cart.offset().left + 10,
                'width': 75,
                'height': 75
            }, 1000, 'easeInOutExpo');

        setTimeout(function () {
            cart.effect("shake", {
                times: 2
            }, 200);
        }, 1500);

        imgclone.animate({
            'width': 0,
            'height': 0
        }, function () {
            $(this).detach()
        });
    }
}

$(document).ready(function () {
    var cart = GetCart();
});

function GetCart()
{
    $.ajax({
        type: "GET",
        url: "/Shop/GetCart",
        success: function (data)
        {
            var count = 0;
            $.each(data, function (index, value) {
                count = count + value.Quantity;
            });
            $(".cartItems").text(count);
        }
    });
}