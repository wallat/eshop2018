﻿using EShop2018.DTOs;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EShop2018.MVC.Models.Product
{
    /// <summary>
    /// ViewModel de Product/Index
    /// </summary>
    public class ProductIndexViewModel
    {
        /// <summary>
        /// Colección de productos
        /// </summary>
        public IEnumerable<ProductDTO> Products { get; set; }

        /// <summary>
        /// Colección de categorias
        /// </summary>
        public IEnumerable<CategoryDTO> Categories { get; set; }

        /// <summary>
        /// Numero total de productos a mostrar
        /// </summary>
        public int ProductCount { get; set; }
    }
}