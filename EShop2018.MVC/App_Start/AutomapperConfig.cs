﻿using AutoMapper;
using EShop2018.DTOs;
using EShop2018.MVC.Models.Account;
using EShop2018.MVC.Models.Cart;
using EShop2018.MVC.Models.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EShop2018.MVC
{
    /// <summary>
    /// Configurador de Autommaper
    /// </summary>
    public static class AutomapperConfig
    {
        /// <summary>
        /// Método para configurar autommaper
        /// </summary>
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<ProductDTO, ProductIndexViewModel>();
                cfg.CreateMap<ShoppingCartLineDTO, CartLineViewModel>();

                //Admin
                cfg.CreateMap<ProductDTO, Areas.Admin.Models.ProductAdmin.ProductViewModel>();
                cfg.CreateMap<Areas.Admin.Models.ProductAdmin.ProductViewModel, ProductDTO>();
                cfg.CreateMap<Areas.Admin.Models.ProductAdmin.ProductViewModel, ProductCreateDTO>();
                cfg.CreateMap<RegisterViewModel, RegisterDTO>();
            });
        }
    }
}