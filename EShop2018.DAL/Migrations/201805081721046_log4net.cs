namespace EShop2018.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class log4net : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Log4NetLog",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Thread = c.String(nullable: false, maxLength: 255),
                        Level = c.String(nullable: false, maxLength: 50),
                        Logger = c.String(nullable: false, maxLength: 255),
                        Message = c.String(nullable: false, maxLength: 4000),
                        Exception = c.String(maxLength: 2000),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Log4NetLog");
        }
    }
}
