namespace EShop2018.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Tax_Id : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Products", "Tax_Id", "dbo.Taxes");
            DropIndex("dbo.Products", new[] { "Tax_Id" });
            AlterColumn("dbo.Products", "Tax_Id", c => c.Int(nullable: false));
            CreateIndex("dbo.Products", "Tax_Id");
            AddForeignKey("dbo.Products", "Tax_Id", "dbo.Taxes", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Products", "Tax_Id", "dbo.Taxes");
            DropIndex("dbo.Products", new[] { "Tax_Id" });
            AlterColumn("dbo.Products", "Tax_Id", c => c.Int());
            CreateIndex("dbo.Products", "Tax_Id");
            AddForeignKey("dbo.Products", "Tax_Id", "dbo.Taxes", "Id");
        }
    }
}
