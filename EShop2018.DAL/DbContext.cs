﻿using EShop2018.CORE.Domain;
using EShop2018.CORE.Interfaces;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop2018.DAL
{
    //Enable-Migrations
    //Add-Migration "name xxxxx" -StartUpProjectName "EShop2018.API" -ConnectionStringName "DefaultConnection" -Verbose
    //Update-Database -StartUpProjectName "EShop2018.API" -ConnectionStringName "DefaultConnection" -Verbose 

    /// <summary>
    /// Contexto de datos de EntityFramework
    /// </summary>
    public class DbContext : IdentityDbContext<ApplicationUser>, IDbContext
    {
        /// <summary>
        /// Constructor para migrations
        /// </summary>
        public DbContext()
            :base("DefaultConnection", false)
        {

        }
        /// <summary>
        /// Constructor con cadena de conexión
        /// </summary>
        /// <param name="nameOrConnectionString">Cadena de conexión</param>
        public DbContext(string nameOrConnectionString) 
            :base(nameOrConnectionString, false)
        {

        }
        #region Colecciones persistibles

        /// <summary>
        /// Colección persistible de Pedidos
        /// </summary>
        public DbSet<Order> Orders { get; set; }

        /// <summary>
        /// Colección persistible de categorias
        /// </summary>
        public DbSet<Category> Categories { get; set; }

        /// <summary>
        /// Colección persistible de productos
        /// </summary>
        public DbSet<Product> Products { get; set; }

        /// <summary>
        /// Colección persistible del carrito
        /// </summary>
        public DbSet<ShoppingCartLine> ShoppingCart { get; set; }

        /// <summary>
        /// Colección persistible de Tasas
        /// </summary>
        public DbSet<Tax> Taxs { get; set; }

        /// <summary>
        /// Colección persistible de log4net
        /// </summary>
        public DbSet<Log4NetLog> Log4NetLogs { get; set; }

        #endregion

        /// <summary>
        /// Especificar excepciones en la creación de la base de datos
        /// </summary>
        /// <param name="modelBuilder">Constructor del modelo</param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Quitar borrado en cascada entre order y direccion de facturacion
            //para evitar referencias cíclicas
            modelBuilder.Entity<Order>().HasRequired(f => f.BillingAddress)
                .WithMany().WillCascadeOnDelete(false);
            //Quitar borrado en cascada entre order y dirección de envio
            //para evitar referencias cíclicas
            modelBuilder.Entity<Order>().HasRequired(f => f.DeliveryAddress)
                .WithMany().WillCascadeOnDelete(false);

            base.OnModelCreating(modelBuilder);
        }
    }
}
