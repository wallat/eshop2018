﻿using EShop2018.CORE.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop2018.CORE.Interfaces
{
    /// <summary>
    /// Interfaz del Manager de categorias
    /// </summary>
    public interface ICategoryManager : IManager<Category>
    {
    }
}
