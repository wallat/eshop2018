﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop2018.CORE.Interfaces
{
    /// <summary>
    /// Interfaz del manager genérico
    /// </summary>
    public interface IManager<T>
    {
        /// <summary>
        /// Contexto de datos
        /// </summary>
        IDbContext Context { get; }

        /// <summary>
        /// Obtener un elemento por su id
        /// </summary>
        /// <param name="id">identificador</param>
        /// <returns>Objeto con el id indicado</returns>
        T GetById(object id);

        /// <summary>
        /// Obtiene una lista de objetos por sus identificadores
        /// </summary>
        /// <param name="ids">Identificadores a buscar</param>
        /// <returns>Lista de objetos</returns>
        IEnumerable<T> GetByIds(List<object> ids);

        /// <summary>
        /// Obtener todos los registros
        /// </summary>
        /// <returns>Lista de todos los registros</returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// Método para eliminar un elemento
        /// </summary>
        /// <param name="element"></param>
        void Remove(T element);

        /// <summary>
        /// Guardar cambios
        /// </summary>
        /// <returns>retorna el número de registros afectados</returns>
        int SaveChanges();

        /// <summary>
        /// Añade un elemento a la colección persistible
        /// </summary>
        /// <param name="element">Elemento para añadir</param>
        void Add(T element);
    }
}
