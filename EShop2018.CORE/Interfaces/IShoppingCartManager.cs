﻿using EShop2018.CORE.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop2018.CORE.Interfaces
{
    /// <summary>
    /// Manager del carrito de la compra
    /// </summary>
    public interface IShoppingCartManager : IManager<ShoppingCartLine>
    {
        /// <summary>
        /// Añade productos al carrito
        /// </summary>
        /// <param name="userId">Identificador de usuario</param>
        /// <param name="sessionId">Identificador de sesion</param>
        /// <param name="productId">Identificador de producto</param>
        /// <param name="quantity">Cantidad a añadir</param>
        /// <returns>Elementos del carrito</returns>
        decimal AddProduct(string userId, string sessionId, int productId, decimal quantity);

        /// <summary>
        /// Obtiene el carrito de un usuario
        /// </summary>
        /// <param name="userId">Identificador de usuario</param>
        /// <param name="sessionId">Identificador de session</param>
        /// <returns></returns>
        IQueryable<ShoppingCartLine> GetShoppingCartByUser(string userId, string sessionId);
    }
}
