﻿using EShop2018.CORE.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop2018.CORE.Interfaces
{
    /// <summary>
    /// Interfaz del manager de tasas
    /// </summary>
    public interface ITaxManager : IManager<Tax>
    {
        /// <summary>
        /// Calcula el precio final de una cantidad
        /// </summary>
        /// <param name="taxId">Identificador de tasa</param>
        /// <param name="price">Precio base sobre el que calcular el impuesto</param>
        /// <returns>Precio final</returns>
        decimal GetFinalPrice(int taxId, decimal price);

        /// <summary>
        /// Calcula el precio neto de una catidad
        /// </summary>
        /// <param name="taxId">Identificador de la tasa</param>
        /// <param name="price">Precio bruto</param>
        /// <returns>Precio neto</returns>
        decimal GetNetPrice(int taxId, decimal price);

        /// <summary>
        /// Calcula el precio neto de una cantidad
        /// </summary>
        /// <param name="tax">Valor de la tasa a descontar</param>
        /// <param name="price">Precio bruto</param>
        /// <returns>precio sin tasa</returns>
        decimal GetNetPrice(decimal tax, decimal price);
    }
}
