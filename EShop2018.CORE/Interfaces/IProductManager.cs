﻿using EShop2018.CORE.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop2018.CORE.Interfaces
{
    /// <summary>
    /// Interfaz del manager de producto
    /// </summary>
    public interface IProductManager : IManager<Product>
    {
        /// <summary>
        /// Obtiene los productos de una categoría
        /// </summary>
        /// <param name="categoryId">Identificador de categoria</param>
        /// <returns>Productos de la categoría indicada</returns>
        IQueryable<Product> GetByCategoryId(int categoryId);
    }
}
