﻿using EShop2018.CORE.Domain;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop2018.CORE.Interfaces
{
    public interface IUserManager : IDisposable
    {
        IIdentityValidator<ApplicationUser> UserValidator { get; set; }
        IIdentityValidator<string> PasswordValidator { get; set; }
        IClaimsIdentityFactory<ApplicationUser, string> ClaimsIdentityFactory { get; set; }
        IIdentityMessageService EmailService { get; set; }
        IIdentityMessageService SmsService { get; set; }
        IUserTokenProvider<ApplicationUser, string> UserTokenProvider { get; set; }
        bool UserLockoutEnabledByDefault { get; set; }
        int MaxFailedAccessAttemptsBeforeLockout { get; set; }
        TimeSpan DefaultAccountLockoutTimeSpan { get; set; }
        IPasswordHasher PasswordHasher { get; set; }
        IDictionary<string, IUserTokenProvider<ApplicationUser, string>> TwoFactorProviders { get; }
    }
}
