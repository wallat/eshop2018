﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop2018.CORE.Interfaces
{
    public interface IRoleManager: IDisposable
    {
        IIdentityValidator<IdentityRole> RoleValidator { get; set; }
        IQueryable<IdentityRole> Roles { get; }        

        Task<IdentityResult> CreateAsync(IdentityRole role);
        Task<IdentityResult> DeleteAsync(IdentityRole role);        
        Task<IdentityRole> FindByIdAsync(string roleId);
        
        Task<IdentityRole> FindByNameAsync(string roleName);
        
        Task<bool> RoleExistsAsync(string roleName);
        Task<IdentityResult> UpdateAsync(IdentityRole role);        
    }
}
