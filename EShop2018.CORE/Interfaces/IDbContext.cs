﻿using System.Data.Entity;
using EShop2018.CORE.Domain;
using System.Data.Entity.Infrastructure;

namespace EShop2018.CORE.Interfaces
{
    /// <summary>
    /// Interfaz del contexto de datos
    /// </summary>
    public interface IDbContext
    {
        /// <summary>
        /// Colección persistible de categorias
        /// </summary>
        DbSet<Category> Categories { get; set; }
        /// <summary>
        /// Colección persistible de pedidos
        /// </summary>
        DbSet<Order> Orders { get; set; }
        /// <summary>
        /// Colección persistible de productos
        /// </summary>
        DbSet<Product> Products { get; set; }
        /// <summary>
        /// Colección persistible del carrito
        /// </summary>
        DbSet<ShoppingCartLine> ShoppingCart { get; set; }
        /// <summary>
        /// Colección persistible de Tasas
        /// </summary>
        DbSet<Tax> Taxs { get; set; }

        /// <summary>
        /// Método para guardar cambios
        /// </summary>
        /// <returns>Registros afectados</returns>
        int SaveChanges();

        /// <summary>
        /// Obtiene la colección de una entidad
        /// </summary>
        /// <typeparam name="T">Entidad de la que queremos el contexto</typeparam>
        /// <returns>Colección de la entidad</returns>
        DbSet<T> Set<T>() where T : class;
        /// <summary>
        /// Obtiene una entrada de una entidad del contexto
        /// </summary>
        /// <typeparam name="T">Tipo de entidad de la que queremos la entrada</typeparam>
        /// <param name="entity">Entidad de la que queremos su entrada</param>
        /// <returns>Entada de la entidad</returns>
        DbEntityEntry<T> Entry<T>(T entity) where T : class;
    }
}