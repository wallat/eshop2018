﻿namespace EShop2018.CORE.Enums
{
    /// <summary>
    /// Enumerado de estados de pedido
    /// </summary>
    public enum OrderStatus
    {
        /// <summary>
        /// Pendiente
        /// </summary>
        Pending,
        /// <summary>
        /// Pagado
        /// </summary>
        Paid,
        /// <summary>
        /// En preparación
        /// </summary>
        InProcess,
        /// <summary>
        /// Enviado
        /// </summary>
        Send,
        /// <summary>
        /// Finalizado
        /// </summary>
        End,
        /// <summary>
        /// Cancelado
        /// </summary>
        Canceled
    }
}