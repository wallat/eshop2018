﻿namespace EShop2018.CORE.Enums
{
    /// <summary>
    /// Enumerado de tipo de documento
    /// </summary>
    public enum DocumentType
    {
        /// <summary>
        /// Tipo imagen
        /// </summary>
        Image,
        /// <summary>
        /// Tipo video
        /// </summary>
        Video,
        /// <summary>
        /// Tipo pdf
        /// </summary>
        Pdf,
        /// <summary>
        /// Tipo word
        /// </summary>
        Word
    }
}