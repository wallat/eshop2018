﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop2018.CORE.Domain
{
    /// <summary>
    /// Clase de dominio de productos
    /// </summary>
    public class Product
    {
        /// <summary>
        /// Identificador
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Nombre del producto
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Descripción breve
        /// </summary>
        public string SortDescription { get; set; }

        /// <summary>
        /// Descripción
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Precio neto del producto
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Stock del producto
        /// </summary>
        public decimal Stock { get; set; }

        /// <summary>
        /// Esta disponible el producto
        /// </summary>
        public bool Available { get; set; }

        /// <summary>
        /// Impuesto asociado
        /// </summary>
        public Tax Tax { get; set; }
        /// <summary>
        /// Identificador de la tasa
        /// </summary>
        [ForeignKey("Tax")]
        public int Tax_Id { get; set; }

        /// <summary>
        /// Categorias del producto
        /// </summary>
        public List<Category> Categories { get; set; }

        /// <summary>
        /// Colección de documentos
        /// </summary>
        public List<Document> Documents { get; set; }

        /// <summary>
        /// El producto es destacado
        /// </summary>
        public bool Featured { get; set; }

        /// <summary>
        /// Indica el % de descuento del producto si lo tiene
        /// </summary>
        public decimal? Discount { get; set; }
    }
}
