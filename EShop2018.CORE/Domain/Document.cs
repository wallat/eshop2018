﻿using EShop2018.CORE.Enums;
using System.ComponentModel.DataAnnotations.Schema;

namespace EShop2018.CORE.Domain
{
    /// <summary>
    /// Clase de dominio de documentos
    /// </summary>
    public class Document
    {
        /// <summary>
        /// Identificador
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Ruta del documento
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// Tipo de documento
        /// </summary>
        public DocumentType Type { get; set; }

        /// <summary>
        /// Orden de visualización de documento
        /// </summary>
        public int Order { get; set; }

        #region Relacion con producto

        /// <summary>
        /// Identificador del producto
        /// </summary>
        [ForeignKey("Product")]
        public int ProductId { get; set; }

        /// <summary>
        /// Objeto del producto relacionado
        /// </summary>
        public Product Product { get; set; }

        #endregion
    }
}