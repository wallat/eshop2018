﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace EShop2018.CORE.Domain
{
    /// <summary>
    /// Clase de dominio de categorias
    /// </summary>
    public class Category
    {
        /// <summary>
        /// Identificador
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Nombre de categoría
        /// </summary>
        public string Name { get; set; }

        #region Relacion con el objeto Padre

        /// <summary>
        /// Identificador de categoria padre
        /// </summary>
        [ForeignKey("Parent")]
        public int? ParentId { get; set; }
        
        /// <summary>
        /// Objeto de categoria padre
        /// </summary>
        public Category Parent { get; set; }

        #endregion  

        /// <summary>
        /// Colección de productos que tiene la categoria
        /// </summary>
        public List<Product> Products { get; set; }
    }
}