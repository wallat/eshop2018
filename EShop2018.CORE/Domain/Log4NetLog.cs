﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop2018.CORE.Domain
{
    /// <summary>
    /// Clase de log4net
    /// </summary>
    [Table("Log4NetLog")]
    public class Log4NetLog
    {
        /// <summary>
        /// Identificador
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Fecha
        /// </summary>
        [Required]
        public DateTime Date { get; set; }
        /// <summary>
        /// Número de hilo de ejecución
        /// </summary>
        [MaxLength(255)]
        [Required]
        public string Thread { get; set; }
        /// <summary>
        /// Nivel de log
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string Level { get; set; }
        /// <summary>
        /// Nombre del manejador de log
        /// </summary>
        [Required]
        [MaxLength(255)]
        public string Logger { get; set; }
        /// <summary>
        /// Mensaje del error
        /// </summary>
        [Required]
        [MaxLength(4000)]
        public string Message { get; set; }
        /// <summary>
        /// Excepción
        /// </summary>
        [MaxLength(2000)]
        public string Exception { get; set; }
    }
}
