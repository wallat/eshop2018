﻿namespace EShop2018.CORE.Domain
{
    /// <summary>
    /// Entidad de dominio de lineas de pedido
    /// </summary>
    public class OrderLine
    {
        /// <summary>
        /// Identificador
        /// </summary>
        public int Id { get; set; }

        #region Datos del producto        

        /// <summary>
        /// Nombre del producto
        /// </summary>
        public string ProductName { get; set; }

        /// <summary>
        /// Precio neto del producto
        /// </summary>
        public decimal ProductPrice { get; set; }

        /// <summary>
        /// Nombre del impuesto aplicado
        /// </summary>
        public string TaxName { get; set; }
        /// <summary>
        /// Porcentaje de la tasa aplicada
        /// </summary>
        public decimal TaxValue { get; set; }        
        /// <summary>
        /// Precio total con iva
        /// </summary>
        public decimal TotalPrice { get; set; }

        #endregion

        /// <summary>
        /// Cantidad de producto
        /// </summary>
        public decimal Quantity { get; set; }                
    }
}