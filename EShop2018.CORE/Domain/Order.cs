﻿using EShop2018.CORE.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop2018.CORE.Domain
{
    /// <summary>
    /// Clase de dominio de pedidos
    /// </summary>
    public class Order
    {
        /// <summary>
        /// Identificador
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Lineas de pedido
        /// </summary>
        public List<OrderLine> OrderLines { get; set; }

        /// <summary>
        /// Estado del pedido
        /// </summary>
        public OrderStatus Status { get; set; }

        #region Fechas
        /// <summary>
        /// Fecha de creación
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Fecha de pago
        /// </summary>
        public DateTime? PaymentDate { get; set; }

        /// <summary>
        /// Fecha de envio
        /// </summary>
        public DateTime? SendDate { get; set; }

        /// <summary>
        /// Fecha de entrega
        /// </summary>
        public DateTime? DeliveryDate { get; set; }

        /// <summary>
        /// Fecha de cancelación
        /// </summary>
        public DateTime? CancellationDate { get; set; }
        #endregion

        #region Relación con el usuario

        /// <summary>
        /// Identificador de usuario
        /// </summary>
        [ForeignKey("User")]
        public string UserId { get; set; }

        /// <summary>
        /// Objeto de usuario
        /// </summary>
        public ApplicationUser User { get; set; }

        #endregion

        #region Relación con la dirección de envio
        /// <summary>
        /// Identificador de la direccón de envio
        /// </summary>
        [ForeignKey("DeliveryAddress")]
        public int DeliveryAddressId { get; set; }
        
        /// <summary>
        /// Objeto de la dirección de envio
        /// </summary>
        public Address DeliveryAddress { get; set; }
        #endregion

        #region Relación con la dirección de facturación
        /// <summary>
        /// Identificador de la direccón de facturación
        /// </summary>
        [ForeignKey("BillingAddress")]
        public int BillingAddressId { get; set; }

        /// <summary>
        /// Objeto de la dirección de facturación
        /// </summary>
        public Address BillingAddress { get; set; }
        #endregion

        /// <summary>
        /// Método de pago
        /// </summary>
        public string MethodPay { get; set; }

        /// <summary>
        /// Método de envio
        /// </summary>
        public string MethodSend { get; set; }
    }
}
