﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop2018.CORE.Domain
{
    /// <summary>
    /// Entidad de dominio de carrito de la compra
    /// </summary>    
    public class ShoppingCartLine
    {
        /// <summary>
        /// Identificador
        /// </summary>
        public int Id { get; set; }

        #region Relación con el usuario

        /// <summary>
        /// Identificador de usuario
        /// </summary>
        [ForeignKey("User")]
        public string UserId { get; set; }
        
        /// <summary>
        /// Objeto de usuario
        /// </summary>
        public ApplicationUser User { get; set; }

        #endregion

        /// <summary>
        /// Sesión de usuario
        /// </summary>
        public string Session { get; set; }

        /// <summary>
        /// Fecha de creación
        /// </summary>
        public DateTime Date { get; set; }

        #region Relación con producto

        /// <summary>
        /// Identificador de producto
        /// </summary>
        [ForeignKey("Product")]
        public int ProductId { get; set; }
        
        /// <summary>
        /// Objeto del producto añadido
        /// </summary>
        public Product Product { get; set; }

        #endregion

        /// <summary>
        /// Cantidad añadida
        /// </summary>
        public decimal Quantity { get; set; }
    }
}
