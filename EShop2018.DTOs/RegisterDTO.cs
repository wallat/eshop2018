﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop2018.DTOs
{
    /// <summary>
    /// DTO de registro
    /// </summary>
    public class RegisterDTO
    {
        public string Email { get; set; }
        public string Password { get; set; }        
        public string ConfirmPassword { get; set; }
    }
}
