﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop2018.DTOs
{
    /// <summary>
    /// DTO de lineas de carrito
    /// </summary>
    public class ShoppingCartLineDTO
    {
        /// <summary>
        /// Identificador de la linea de carrito
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Nombre del producto
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Cantidad
        /// </summary>
        public decimal Quantity { get; set; }

        /// <summary>
        /// Precio unidad con impuestos incluidos
        /// </summary>
        public decimal Price { get; set; }
    }
}
