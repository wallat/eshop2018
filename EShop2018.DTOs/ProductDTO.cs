﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop2018.DTOs
{
    /// <summary>
    /// DTO de producto
    /// </summary>
    public class ProductDTO
    {
        /// <summary>
        /// Identificador del producto
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Nombre del producto
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Ruta de la imagen
        /// </summary>
        public string Image { get; set; }

        /// <summary>
        /// Precio Final del producto
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Precio sin descuento
        /// </summary>
        public decimal? OldPrice { get; set; }

        /// <summary>
        /// Indica si el producto está en promoción
        /// </summary>
        public bool Featured { get; set; }
    }
}
