﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop2018.DTOs
{
    /// <summary>
    /// DTO para crear y editar productos
    /// </summary>
    public class ProductCreateDTO : ProductDTO
    {
        /// <summary>
        /// Identificador de impuesto
        /// </summary>
        public int Tax_Id { get; set; }
        /// <summary>
        /// Categorias que tiene el producto
        /// </summary>
        public List<int> Categories { get; set; }
    }
}
