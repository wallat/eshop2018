﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop2018.DTOs
{
    /// <summary>
    /// Dto de Categorias
    /// </summary>
    public class CategoryDTO
    {
        /// <summary>
        /// Identificador
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Nombre de categoría
        /// </summary>
        public string Name { get; set; }        

        /// <summary>
        /// Identificador de categoria padre
        /// </summary>
        public int ParentId { get; set; }
        
    }
}
