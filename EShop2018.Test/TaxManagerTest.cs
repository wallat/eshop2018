﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EShop2018.Application;
using EShop2018.CORE.Domain;
using System.Collections.Generic;
using Moq;
using System.Data.Entity;
using EShop2018.CORE.Interfaces;
using System.Linq;

namespace EShop2018.Test
{
    [TestClass]
    public class TaxManagerTest
    {
        [TestMethod]
        public void GetNetPriceTest()
        {
            //Arrange
            var target = new TaxManager(GetContext());

            //Act
            var result = target.GetNetPrice((decimal)10, (decimal)110);

            //Assert
            Assert.AreEqual(100, result);
        }

        [TestMethod]
        public void GetNetPriceTestDb()
        {
            //Arrange            
            var target = new TaxManager(GetContext());

            //Act
            var result = target.GetNetPrice(2, 110);

            //Assert
            Assert.AreEqual(100, result);
        }

        [TestMethod]
        public void GetAllTest()
        {
            //Arrange           
            var target = new TaxManager(GetContext());

            //Act
            var result = target.GetAll();

            //Assert
            Assert.AreEqual(3, result.Count());
        }

        private IDbContext GetContext()
        {
            var context = new Mock<IDbContext>();
            #region Tax
            var dataTax = new List<Tax>
            {
                new Tax{ Id = 1, Name = "Superreducido", Percent = 7 },
                new Tax{ Id = 2, Name = "Reducido", Percent = 10 },
                new Tax{ Id = 3, Name = "Normal", Percent = 21 }
            };

            var setTax = new Mock<DbSet<Tax>>().SetupData(dataTax);

            setTax.Setup(m => m.Find(It.IsAny<object[]>()))
                .Returns<object[]>(ids => dataTax.FirstOrDefault(d => d.Id == (int)ids[0]));

            context.Setup(c => c.Taxs).Returns(setTax.Object);
            context.Setup(c => c.Set<Tax>()).Returns(setTax.Object);
            #endregion

            #region Category
            var dataCategory = new List<Category>
            {

            };

            var setCategory = new Mock<DbSet<Category>>().SetupData(dataCategory);

            setCategory.Setup(m => m.Find(It.IsAny<object[]>()))
                .Returns<object[]>(ids => dataCategory.FirstOrDefault(d => d.Id == (int)ids[0]));

            context.Setup(c => c.Categories).Returns(setCategory.Object);
            context.Setup(c => c.Set<Category>()).Returns(setCategory.Object);

            #endregion
            
            return context.Object;
        }
    }
}
